
package BankApp;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mert
 */
public class NewAccountsReader {
  
    
    public static List<String[]> reader(){
   
        List<String[]> data =new LinkedList<String[]>();
        String dataTaker = "";
        try {
            Scanner scanner = new Scanner(new FileReader("text.csv"));
            
          while(scanner.hasNext()){
                dataTaker = scanner.nextLine();
                String[]dataRecord =dataTaker.split(",");
                data.add(dataRecord);
                
            
            }
        }
        catch (FileNotFoundException ex) {
            System.out.println("Could not find file ");
        }
             
         return data;
}
    
}


package BankApp;

import java.util.Random;

/**
 *
 * @author Mert
 */
public abstract class Account implements BaseRate {
    private String firstName;
    private String lastName;
    private String SSN;
    private double balance;      
    protected double rate;
    protected String AccountNumber;
    private static int Number = 10000;
    
    
    public Account(String firstName,String lastName,String SSN,double InitialDeposit){
        
        this.firstName=firstName;
        this.lastName=lastName;
        this.SSN=SSN;
        this.balance = InitialDeposit;
        
        
        this.AccountNumber = setAccountNumber();
        SetRate();
        
    }
    public String setAccountNumber(){
        String LasttwoDigitSSN = SSN.substring(SSN.length()-2, SSN.length());
        int SpecialNumber = Number;
        Number++;
        Random rand = new Random();
        int RandNumber = rand.nextInt(1000) + 100;
        
        return LasttwoDigitSSN + SpecialNumber + RandNumber;
    }
    
    public void deposit(int blc){
        this.balance += blc;
        System.out.println("New balance = " + balance);
    }
    
    public void withdraw(int blc){
        this.balance-=blc;
        System.out.println("New Balance = " + balance );
    }
    
    public void transver(Checking a1 , Saving a2, int bakiye){
       a1.setBalance(a1.getBalance() - bakiye);
       a2.setBalance(a2.getBalance() + bakiye);
        System.out.println("New Balances = " + a1.getBalance() + ",New Balance " +a2.getBalance());
    }

   public void showInfo(){
       System.out.println("Name = "  +firstName + " " + lastName + "\n"
                          +"Account Number = " + AccountNumber + "\nBalance = " + balance);
   }

    public abstract void SetRate();

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    
    
}
